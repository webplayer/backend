package com.webplayer.playlists;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonInclude;
import com.vladmihalcea.hibernate.type.array.StringArrayType;
import com.webplayer.database.User;
import lombok.Data;
import lombok.NoArgsConstructor;
import org.hibernate.annotations.Type;
import org.hibernate.annotations.TypeDef;
import org.hibernate.annotations.TypeDefs;

import javax.persistence.*;

@TypeDefs({@TypeDef(name = "string-array", typeClass = StringArrayType.class)})
@Data
@Entity
@NoArgsConstructor
@Table(name = "playlists_new")
@JsonInclude(JsonInclude.Include.NON_NULL)
public class Playlist {
    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    Long id;

    String source;
    String external_id;
    boolean dynamic;

    String title;

    @Column(columnDefinition = "TEXT")
    String thumbnail;

    @Transient
    Integer size;

    @Type(type = "string-array")
    @Column(name = "songs", columnDefinition = "text[]")
    @JsonIgnore
    String[] songs;

    @ManyToOne
    @JsonIgnore
    User user;

    // database
    public Playlist(Long id, String source, String external_id, boolean dynamic, String title, String thumbnail, String[] songs) {
        this.id = id;
        this.source = source;
        this.external_id = external_id;
        this.dynamic = dynamic;
        this.title = title;
        this.thumbnail = thumbnail;
        this.size = songs == null ? null : songs.length;
    }

    // custom
    public Playlist(String source, String external_id, boolean dynamic, String title, String thumbnail) {
        this.source = source;
        this.external_id = external_id;
        this.dynamic = dynamic;
        this.title = title;
        this.thumbnail = thumbnail;
    }

    // simple external
    public Playlist(String source, String external_id, String title) {
        this.source = source;
        this.external_id = external_id;
        this.dynamic = true;
        this.title = title;
    }
}
