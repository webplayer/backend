package com.webplayer.playlists;

import com.webplayer.database.DBCacheRepository;
import com.webplayer.database.DBService;
import com.webplayer.database.DbPlaylistRepository;
import com.webplayer.database.User;
import com.webplayer.objects.endpoints.PlaylistEndpoints;
import com.webplayer.unify.ServiceSelector;
import com.webplayer.unify.UService;
import com.webplayer.unify.page.MainPage;
import com.webplayer.unify.page.containers.AudioType;
import com.webplayer.unify.page.containers.BaseType;
import com.webplayer.unify.page.containers.TypeFactory;
import com.webplayer.unify.page.objects.Audio;
import lombok.RequiredArgsConstructor;
import org.springframework.web.bind.annotation.*;

import java.util.Collections;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

@RestController
@RequiredArgsConstructor
public class PlaylistsController {
    private final DbPlaylistRepository repository;
    private final DBService dbService;
    private final DBCacheRepository dbCacheRepository;
    private final ServiceSelector serviceSelector;


    @GetMapping(PlaylistEndpoints.all)
    public List<Playlist> getAllPlaylists(@CookieValue String token) throws Exception {
        User user = dbService.getUser(token);
        List<Playlist> playlists = repository.getUserPlaylists(user);

        Playlist playlist = new Playlist();
        playlist.setId(0L);
        playlist.setTitle("Помойка");
        playlist.setThumbnail("https://www.astromeridian.ru/assets/images/sonnik/pomoika.jpg");
        playlist.setDynamic(true);
        playlists.add(playlist);

        return playlists;
    }

    @GetMapping(PlaylistEndpoints.get)
    public MainPage getPlaylist(@CookieValue String token, Long id) throws Exception {
        User user = dbService.getUser(token);

        if (id == 0L) {
            MainPage page = new MainPage();
            List<BaseType> playlists = page.getData();
            for (Map.Entry<String, UService> entry : serviceSelector.entrySet()) {
                try {
                    List<Playlist> list = entry.getValue().getMyPlaylists(user);
                    if (list != null) list.forEach(playlist -> playlists.add(TypeFactory.get(playlist)));
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }
            return page;
        }

        Playlist playlist = repository.getByUserAndId(user, id);
        if (playlist.dynamic && playlist.source != null && playlist.external_id != null) {
            return serviceSelector.get(playlist.source).getPlaylist(user, playlist.external_id);
        }

        MainPage page = new MainPage();
        List<Audio> playlistAudios = dbCacheRepository.getPlaylistAudios(user.getId(), id);
        Collections.reverse(playlistAudios);
        page.setData(playlistAudios.stream().map(AudioType::new).collect(Collectors.toList()));
        return page;
    }

    @PostMapping(PlaylistEndpoints.create)
    public void createNewPlaylist(@CookieValue String token, @RequestParam String title, String thumbnail) throws Exception {
        User user = dbService.getUser(token);

        Playlist playlist = new Playlist();
        playlist.setDynamic(false);
        playlist.setUser(user);
        playlist.setTitle(title);
        playlist.setThumbnail(thumbnail);

        repository.save(playlist);
    }

    @PostMapping(PlaylistEndpoints.delete)
    public void deletePlaylist(@CookieValue String token, @RequestParam Long id) throws Exception {
        User user = dbService.getUser(token);
        repository.deleteByUserAndId(user, id);
    }

    @PostMapping(PlaylistEndpoints.update)
    public void updatePlaylist(@CookieValue String token, @RequestParam Long id, String title, String thumbnail) throws Exception {
        User user = dbService.getUser(token);
        Playlist playlist = repository.getByUserAndId(user, id);
        if (title != null) playlist.setTitle(title);
        if (thumbnail != null) playlist.setThumbnail(thumbnail);
        repository.save(playlist);
    }

    @PostMapping(PlaylistEndpoints.addTo)
    public void addToPlaylist(@CookieValue String token, @RequestParam Long id, String s, String audio_id) throws Exception {
        User user = dbService.getUser(token);
        Audio audio = serviceSelector.get(s).getAudioById(user, audio_id);

        if (audio == null) throw new Exception();

        dbCacheRepository.save(audio);
        repository.removeFromArray(user.getId(), id, audio.getId());
        repository.pushToArray(user.getId(), id, audio.getId());
    }

    @PostMapping(PlaylistEndpoints.removeFrom)
    public void removeFromPlaylist(@CookieValue String token, @RequestParam Long id, @RequestParam String audio) throws Exception {
        User user = dbService.getUser(token);

        repository.removeFromArray(user.getId(), id, audio);
    }

    @GetMapping(PlaylistEndpoints.external)
    public MainPage getExternalPlaylist(@CookieValue(required = false) String token, @RequestParam String s, @RequestParam String external_id) throws Exception {
        User user = dbService.getUserNoException(token);
        return serviceSelector.get(s).getPlaylist(user, external_id);
    }

    @PostMapping(PlaylistEndpoints.external)
    public void addExternalPlaylist(@CookieValue String token, @RequestParam String s, @RequestParam String external_id, @RequestParam String title) throws Exception {
        User user = dbService.getUser(token);
        serviceSelector.get(s).addPlaylist(user, external_id, title);
    }

}
