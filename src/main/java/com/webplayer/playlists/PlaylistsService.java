package com.webplayer.playlists;

import com.webplayer.database.DbPlaylistRepository;
import com.webplayer.database.User;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
@RequiredArgsConstructor
public class PlaylistsService {
    private final DbPlaylistRepository playlistRepository;

    public List<Playlist> getUserPlaylists(User user) {
        return playlistRepository.getUserPlaylists(user);
    }

    public Playlist getFullPlaylist(User user, Long id) {
        return playlistRepository.getByUserAndId(user, id);
    }

    public void addPlaylist(Playlist playlist) {
        playlistRepository.save(playlist);
    }
}
