package com.webplayer.youtube;

import com.fasterxml.jackson.core.JsonPointer;
import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.webplayer.objects.endpoints.Endpoints;
import com.webplayer.objects.endpoints.YoutubeEndpoints;
import com.webplayer.objects.utils.DownloadUtil;
import com.webplayer.unify.formats.Format;
import com.webplayer.unify.page.MainPage;
import com.webplayer.unify.page.containers.BaseType;
import com.webplayer.unify.page.containers.TypeFactory;
import com.webplayer.unify.page.objects.Audio;
import com.webplayer.unify.page.objects.Correction;
import com.webplayer.unify.page.objects.PlaylistInfo;
import com.webplayer.unify.params.search.YoutubeSearchParams;
import com.webplayer.youtube.parsers.ChannelParser;
import com.webplayer.youtube.parsers.PlaylistParser;
import com.webplayer.youtube.parsers.VideoParser;
import lombok.extern.slf4j.Slf4j;
import org.apache.commons.lang3.StringUtils;
import org.apache.http.HttpResponse;
import org.apache.http.client.HttpClient;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.impl.client.HttpClients;
import org.springframework.stereotype.Repository;

import java.io.IOException;
import java.net.URLDecoder;
import java.net.URLEncoder;
import java.nio.charset.StandardCharsets;
import java.util.LinkedList;
import java.util.List;
import java.util.concurrent.CompletableFuture;
import java.util.function.UnaryOperator;

@Repository
@Slf4j
public class YoutubeRepository {
    private final HttpClient httpClient = HttpClients.createDefault();
    private final ObjectMapper mapper = new ObjectMapper();
    final String musicChannelId = "UC-9-kyTW8ZkZNDHQJ6FgpwQ";
    private final String xYoutubeClientVersion;

    public YoutubeRepository() {
        String version = "2.20200305.05.00";  //const, if failed
        try {
            String page = DownloadUtil.loadPageApache("https://www.youtube.com");
            String v = StringUtils.substringBetween(page, "INNERTUBE_CONTEXT_CLIENT_VERSION: \"", "\"");
            version = v.startsWith("1") ? "2" + v.substring(1) : v;
            log.info("Youtube client version: " + version);
        } catch (Exception e) {
            log.error("Youtube get version error!", e);
        }
        xYoutubeClientVersion = version;
    }


    private JsonNode apiRequest(String url) throws IOException {
        HttpGet getRequest = new HttpGet(url);
        getRequest.setHeader("x-youtube-client-name", "1");
        getRequest.setHeader("x-youtube-client-version", xYoutubeClientVersion);
        HttpResponse response = httpClient.execute(getRequest);
        return mapper.readTree(response.getEntity().getContent());
    }

    public String getLink(String id) throws Exception {
        return getLinkFromPlayer(getPlayer(id));
    }

    public List<Format> getFormats(String id) throws Exception {
        return getAllFormatsFromPlayer(getPlayer(id));
    }

    public JsonNode getPlayer(String id) throws Exception {
        HttpGet getRequest = new HttpGet("https://www.youtube.com/watch?v=" + id + "&has_verified=1&pbj=1");
        getRequest.setHeader("x-youtube-client-name", "1");
        getRequest.setHeader("x-youtube-client-version", xYoutubeClientVersion);
        //getRequest.setHeader("x-youtube-identity-token", "QUFFLUhqbWFfV0gtLV9feDhmeDgzVVlZMDNGQ2d4VkFsd3w=");
        //getRequest.setHeader("cookie", "VISITOR_INFO1_LIVE=FZ4OlpTu_fM; PREF=cvdm=grid&f5=30000&al=ru&f1=50000000; SID=qQfY1YAkFlfUGILLtQAjB4m_AisLFBHC04pjH8I5hFt1RYBMi0Jd8kiWl7JSXQMZSXRwZw.; HSID=AFcR7zwKla1TJYV-o; SSID=A_P_fHKgyNwgFfT0v; APISID=wsu3lemu8BgrG-xL/AKdee5gWUK1OfR5gm; SAPISID=9fYVgn1fWXIHQWRH/AnAhny2tlnJU71SL7; YSC=vShn8zSpUio; LOGIN_INFO=AFmmF2swRQIgZbSxuW0lBFPsyz8WTQx-IMQQUVrWrCkd1-Xekd09aTkCIQCtb2mHicUBZ762BSlj5LUSIkjJW4I3PK-_ng_4cD6LhQ:QUQ3MjNmemtRelVobENLTnh4Qm5qb3lNUklVdElnOXplM21mNFU3XzNFazFQeDZscU5lRUdvdmw2MVUwODdMUW5yVEFSN2tSYXFKbHFnQlo1QnpoRHJ3S0xMVGUtSWdvalFaUnhBVGdnUWl6MFhJbU5Ebi1IbnZ5d3BMYm1Ud2Zhb0Q4cWFlampOVURVM0VTVjNqdzVPaExvbFItekduVm04RWRsY3V3T2QzOF8zY241RVRUY0dv; SIDCC=AN0-TYsmQ0_C4mDfH84TG082Ppjbzpb2ym8HMDfbRwBvIg10IRXN4rt6fQbuMpftREyvgXA6F6w; ST-1h4g48=itct=IhMIoqOv692E5gIVxNPBCh0XOwNv&csn=bXTbXeKtJ8fwyQXK2Kq4Bg");
        HttpResponse response = httpClient.execute(getRequest);
        return mapper.readTree(response.getEntity().getContent()).get(2).get("player");
    }

    public Audio getAudioById(String id) throws Exception {
        JsonNode main = apiRequest("https://www.youtube.com/watch?v=" + id + "&pbj=1");

        JsonNode videoDetails = main.get(3).get("playerResponse").get("videoDetails");

        Audio audio = new Audio();
        audio.setSource("youtube");
        audio.setId(videoDetails.get("videoId").asText());
        audio.setTitle(videoDetails.get("title").asText());
        audio.setArtist(videoDetails.get("author").asText());
        audio.setLength(videoDetails.get("lengthSeconds").asInt());
        JsonNode thumbnails = videoDetails.get("thumbnail").get("thumbnails");
        audio.setThumbnail(thumbnails.get(thumbnails.size() - 1).get("url").asText());

        return audio;
    }

    public MainPage search(YoutubeSearchParams params) throws IOException {
        MainPage page = new MainPage();
        List<BaseType> list = page.getData();

        String flag = params.videos ?
                params.correction ? "&sp=EgIQAQ%253D%253D" : "&sp=EgIQAUICCAE%253D" :
                params.correction ? "" : "&sp=QgIIAQ%253D%253D";

        boolean first = params.token == null;

        String url = "https://www.youtube.com/results?search_query=" +
                URLEncoder.encode(params.q, StandardCharsets.UTF_8) + flag + "&pbj=1";

        if (!first) url += "&ctoken=" + params.token + "&continuation=" + params.token;
        JsonNode response = apiRequest(url).get(1).get("response");

        try {
            JsonNode main = first ?
                    response.get("contents").get("twoColumnSearchResultsRenderer").get("primaryContents")
                            .get("sectionListRenderer").get("contents").get(0).get("itemSectionRenderer") :
                    response.get("continuationContents").get("itemSectionContinuation");
            JsonNode firstElem = main.get("contents").get(0);

            if (first) {
                if (firstElem.has("showingResultsForRenderer")) { // auto correction
                    list.add(TypeFactory.get(new Correction(params.q, firstElem.get("showingResultsForRenderer")
                            .get("correctedQuery").get("runs").get(0).get("text").asText())));
                }
            } else {
                if (firstElem.has("messageRenderer")) { // no more results
                    page.setNext(null);
                    return page;
                }
            }

            for (JsonNode elem : main.get("contents")) {
                try {
                    if (elem.has("videoRenderer")) {
                        list.add(new VideoParser(elem.get("videoRenderer")).get());
                    } else if (elem.has("channelRenderer")) {
                        list.add(new ChannelParser(elem.get("channelRenderer")).get());
                    } else if (elem.has("playlistRenderer")) {
                        list.add(new PlaylistParser(elem.get("playlistRenderer")).get());
                    }
                } catch (Exception e) {
                    System.out.println(elem);
                    e.printStackTrace();
                }
            }

            params.setToken(main.has("continuations") ?
                    main.get("continuations").get(0).get("nextContinuationData").get("continuation").asText() : null);

            page.setNext(Endpoints.search + "?" + "s=youtube&" + params.toRequestString());
        } catch (Exception e) {
            System.out.println(response);
            e.printStackTrace();
        }
        return page;
    }

    private final JsonPointer channelPointer = JsonPointer.compile("/contents/twoColumnBrowseResultsRenderer/tabs/2" +
            "/tabRenderer/content/sectionListRenderer/contents/0/itemSectionRenderer/contents/0/gridRenderer");

    public MainPage getChannelAudios(String identifier, boolean first) throws Exception {
        if (!first) return getPlaylistAudios(identifier, false);

        CompletableFuture<MainPage> dataFuture = CompletableFuture.supplyAsync(() -> {
            try {
                return getPlaylistAudios("UU" + identifier.substring(2), true);
            } catch (IOException e) {
                e.printStackTrace();
            }
            return null;
        });

        List<BaseType> list = new LinkedList<>();
        String title = null;
        String thumbnail = null;
        try {
            JsonNode response = apiRequest("https://www.youtube.com/channel/" +
                    identifier + "/playlists?view=1&sort=dd&shelf_id=0&pbj=1").get(1).get("response");

            try {
                JsonNode playlistNode = response.get("header").elements().next();
                title = playlistNode.get("title").asText();
                thumbnail = playlistNode.get("avatar").findValue("url").asText();
            } catch (Exception e) {
                e.printStackTrace();
            }


            JsonNode main = response.at(channelPointer);

            for (JsonNode elem : main.get("items")) {
                elem = elem.get("gridPlaylistRenderer");
                list.add(new PlaylistParser(elem) {
                    @Override
                    public String getTitle() {
                        return this.elem.get("title").get("runs").get(0).get("text").asText();
                    }

                    @Override
                    public String getThumbnail() {
                        return this.elem.get("thumbnail").get("thumbnails").get(0).get("url").asText();
                    }

                    @Override
                    public Integer getSize() {
                        return this.elem.get("videoCountShortText").get("simpleText").asInt();
                    }
                }.get());
            }

        } catch (Exception e) {
            e.printStackTrace();
        }

        MainPage page = dataFuture.get();

        try {
            PlaylistInfo info = page.getInfo();
            if (title != null) info.setTitle(title);
            if (thumbnail != null) info.setThumbnail(thumbnail);
            if (list.size() > 0) info.setPlaylists(list);
        } catch (Exception e) {
            e.printStackTrace();
        }

        return page;
    }

    private JsonPointer playlistInfoPointer = JsonPointer.compile("/sidebar/playlistSidebarRenderer/items/0/playlistSidebarPrimaryInfoRenderer");

    public MainPage getPlaylistAudios(String identifier, boolean first) throws IOException {
        MainPage page = new MainPage();

        JsonNode response = apiRequest(first ?
                "https://www.youtube.com/playlist?list=" + identifier + "&pbj=1" :
                "https://www.youtube.com/browse_ajax?ctoken=" + identifier + "&continuation=" + identifier)
                .get(1).get("response");

        try {
            JsonNode main = first ?
                    response.get("contents").get("twoColumnBrowseResultsRenderer").get("tabs")
                            .get(0).get("tabRenderer").get("content").get("sectionListRenderer").get("contents").get(0)
                            .get("itemSectionRenderer").get("contents").get(0).get("playlistVideoListRenderer") :
                    response.get("continuationContents").get("playlistVideoListContinuation");

            if (first) {
                try {
                    JsonNode playlistNode = response.at(playlistInfoPointer);
                    PlaylistInfo info = new PlaylistInfo();
                    info.setTitle(playlistNode.get("title").get("runs").get(0).get("text").asText());
                    JsonNode thumbnails = playlistNode.findValue("thumbnails");
                    info.setThumbnail(thumbnails.get(thumbnails.size() - 1).get("url").asText());
                    page.setInfo(info);
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }

            String nextToken = main.has("continuations") ?
                    main.get("continuations").get(0).get("nextContinuationData").get("continuation").asText() : null;
            page.setNext(YoutubeEndpoints.getPlaylistContinue(nextToken));
            List<BaseType> audioList = page.getData();

            for (JsonNode elem : main.get("contents")) {
                try {
                    elem = elem.get("playlistVideoRenderer");
                    if (!elem.has("shortBylineText")) continue; // private video

                    audioList.add(new VideoParser(elem) {
                        @Override
                        public String getTitle() {
                            return this.elem.get("title").get("simpleText").asText();
                        }
                    }.get());
                } catch (Exception e) {
                    System.out.println(elem);
                    e.printStackTrace();
                }
            }
        } catch (Exception e) {
            System.out.println(response);
            e.printStackTrace();
        }
        return page;
    }

    private List<Format> getAllFormatsFromPlayer(JsonNode player) throws Exception {
        String js = player.get("assets").get("js").asText();
        JsonNode formats = mapper.readTree(player.get("args").get("player_response").asText());
        formats = formats.get("streamingData").get("adaptiveFormats");

        UnaryOperator<String> decipher = SigSolver.getInstance(js);
        List<Format> list = new LinkedList<>();

        for (int i = formats.size() - 1; i >= 0; i--) {
            try {
                JsonNode format = formats.get(i);
                String url = null;

                if (format.hasNonNull("url")) {
                    url = format.get("url").asText();
                } else if (format.hasNonNull("cipher")) {
                    String cipher = format.get("cipher").asText();

                    String[] params = cipher.split("&");
                    String s = "";
                    url = "";

                    for (String p : params) {
                        if (p.startsWith("s=")) {
                            s = URLDecoder.decode(p.substring(2), StandardCharsets.UTF_8);
                        } else if (p.startsWith("url=")) {
                            url = URLDecoder.decode(p.substring(4), StandardCharsets.UTF_8);
                        }
                    }

                    url = url + "&sig=" + URLEncoder.encode(decipher.apply(s), StandardCharsets.UTF_8);
                }

                String quality = format.get("mimeType").asText().startsWith("audio") ? (format.get("averageBitrate").asInt() / 1000) + "k" : format.get("qualityLabel").asText();
                list.add(new Format(format.get("mimeType").asText(), quality, url));
            } catch (Exception e) {
                e.printStackTrace();
            }
        }

        return list;
    }

    private String getLinkFromPlayer(JsonNode player) throws Exception {
        String js = player.get("assets").get("js").asText();
        JsonNode formats = mapper.readTree(player.get("args").get("player_response").asText());
        formats = formats.get("streamingData").get("adaptiveFormats");

        for (int i = formats.size() - 1; i >= 0; i--) {
            JsonNode format = formats.get(i);
            if (!format.get("mimeType").asText().startsWith("audio")) continue;

            if (format.hasNonNull("url")) {
                return format.get("url").asText();
            } else if (format.hasNonNull("cipher")) {
                String cipher = format.get("cipher").asText();

                String[] params = cipher.split("&");
                String s = "";
                String url = "";

                for (String p : params) {
                    if (p.startsWith("s=")) {
                        s = URLDecoder.decode(p.substring(2), StandardCharsets.UTF_8);
                    } else if (p.startsWith("url=")) {
                        url = URLDecoder.decode(p.substring(4), StandardCharsets.UTF_8);
                    }
                }

                return url + "&sig=" + URLEncoder.encode(SigSolver.solve(s, js), StandardCharsets.UTF_8);
            }
        }

        return null;
    }

    public MainPage getMusicChannelPlaylists() throws IOException {
        MainPage mainPage = new MainPage();

        JsonNode response = apiRequest("https://www.youtube.com/channel/" + musicChannelId + "?pbj=1") // gl=Jp for changing country
                .get(1).get("response");
        try {
            JsonNode main = response.get("contents").get("twoColumnBrowseResultsRenderer").get("tabs").get(0)
                    .get("tabRenderer").get("content").get("sectionListRenderer");

            List<BaseType> list = mainPage.getData();
            for (JsonNode node : main.get("contents")) {
                try {
                    node = node.get("itemSectionRenderer").get("contents").get(0).get("shelfRenderer");
                    MainPage elementPage = new MainPage();
                    elementPage.getMap().put("title", node.get("title").get("runs").get(0).get("text").asText());
                    List<BaseType> elementList = elementPage.getData();
                    for (JsonNode elem : node.get("content").get("horizontalListRenderer").get("items")) {
                        try {
                            if (elem.has("compactStationRenderer")) {
                                elem = elem.get("compactStationRenderer");
                                elementList.add(new PlaylistParser(elem) {
                                    @Override
                                    public String getId() {
                                        return this.elem.get("navigationEndpoint").get("watchEndpoint").get("playlistId").asText();
                                    }

                                    @Override
                                    public String getThumbnail() {
                                        JsonNode array = elem.get("thumbnail").get("thumbnails");
                                        return array.get(array.size() - 1).get("url").asText();
                                    }

                                    @Override
                                    public Integer getSize() {
                                        return Integer.parseInt(this.elem.get("videoCountText").get("runs").get(0).get("text").asText().split("\u00a0", 2)[0]);
                                    }
                                }.get());
                            } else if (elem.has("gridPlaylistRenderer")) {
                                elem = elem.get("gridPlaylistRenderer");
                                elementList.add(new PlaylistParser(elem) {
                                    @Override
                                    public String getTitle() {
                                        return this.elem.get("title").get("runs").get(0).get("text").asText();
                                    }

                                    @Override
                                    public String getThumbnail() {
                                        return this.elem.get("thumbnail").get("thumbnails").get(0).get("url").asText();
                                    }

                                    @Override
                                    public Integer getSize() {
                                        return this.elem.get("videoCountShortText").get("simpleText").asInt();
                                    }
                                }.get());
                            }


                        } catch (Exception e) {
                            System.out.println(elem);
                            e.printStackTrace();
                        }
                    }
                    list.add(TypeFactory.get(elementPage));
                } catch (Exception e) {
                    System.out.println(node);
                    e.printStackTrace();
                }
            }

        } catch (Exception e) {
            System.out.println(response);
            e.printStackTrace();
        }

        return mainPage;
    }

}
