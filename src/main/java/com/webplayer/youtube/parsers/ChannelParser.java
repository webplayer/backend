package com.webplayer.youtube.parsers;

import com.fasterxml.jackson.databind.JsonNode;

public class ChannelParser extends BasicPlaylistParser {
    public ChannelParser(JsonNode elem) {
        super(elem);
    }

    @Override
    public String getId() {
        return elem.get("channelId").asText();
    }

    @Override
    public String getTitle() {
        return elem.get("title").get("simpleText").asText();
    }

    @Override
    public String getThumbnail() {
        return elem.get("thumbnail").get("thumbnails").get(0).get("url").asText();
    }

    @Override
    public Integer getSize() {
        JsonNode sizeNode = elem.get("videoCountText").get("runs");
        return sizeNode.size() > 1 ? sizeNode.get(0).get("text").asInt() : Integer.parseInt(sizeNode.get(0).get("text").asText().split(" ")[0]);
    }
}
