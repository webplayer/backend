package com.webplayer.youtube.parsers;

import com.fasterxml.jackson.databind.JsonNode;
import com.webplayer.playlists.Playlist;
import com.webplayer.unify.page.containers.TypeFactory;
import lombok.RequiredArgsConstructor;

@RequiredArgsConstructor
public abstract class BasicPlaylistParser {
    public final JsonNode elem;

    public abstract String getId();
    public abstract String getTitle();
    public abstract String getThumbnail();
    public abstract Integer getSize();

    public com.webplayer.unify.page.containers.PlaylistType get() {
        Playlist playlist = new Playlist("youtube", getId(), getTitle());
        playlist.setThumbnail(getThumbnail());
        playlist.setSize(getSize());
        return TypeFactory.get(playlist);
    }
}
