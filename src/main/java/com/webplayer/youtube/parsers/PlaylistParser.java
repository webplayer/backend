package com.webplayer.youtube.parsers;

import com.fasterxml.jackson.databind.JsonNode;

public class PlaylistParser extends BasicPlaylistParser {
    public PlaylistParser(JsonNode elem) {
        super(elem);
    }

    @Override
    public String getId() {
        return elem.get("playlistId").asText();
    }

    @Override
    public String getTitle() {
        return elem.get("title").get("simpleText").asText();
    }

    @Override
    public String getThumbnail() {
        JsonNode array = elem.get("thumbnails").get(0).get("thumbnails");
        return array.get(array.size() - 1).get("url").asText();
    }

    @Override
    public Integer getSize() {
        return elem.get("videoCount").asInt();
    }
}
