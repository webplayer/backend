package com.webplayer.youtube.parsers;

import com.fasterxml.jackson.databind.JsonNode;
import com.webplayer.unify.page.containers.TypeFactory;
import com.webplayer.objects.utils.ParseUtils;
import com.webplayer.unify.page.containers.AudioType;
import com.webplayer.unify.page.objects.Audio;
import lombok.RequiredArgsConstructor;

@RequiredArgsConstructor
public class VideoParser {
    public final JsonNode elem;

    public String getId() {
        return elem.get("videoId").asText();
    }
    public String getTitle() {
        return elem.get("title").get("runs").get(0).get("text").asText();
    }
    public String getArtist() {
        return elem.get("shortBylineText").get("runs").get(0).get("text").asText();
    }
    public String getThumbnail() {
        JsonNode thumbArr = elem.get("thumbnail").get("thumbnails");
        return thumbArr.get(thumbArr.size() - 1).get("url").asText();
    }
    public Integer getLength() {
        return ParseUtils.convertTimeString(elem.get("thumbnailOverlays").get(0)
                .get("thumbnailOverlayTimeStatusRenderer").get("text").get("simpleText").asText(), ":");
    }

    public AudioType get() {
        return TypeFactory.get(new Audio(this));
    }
}
