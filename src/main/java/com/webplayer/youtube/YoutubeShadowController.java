package com.webplayer.youtube;


import com.webplayer.database.DBService;
import com.webplayer.objects.endpoints.YoutubeEndpoints;
import com.webplayer.playlists.PlaylistsService;
import com.webplayer.unify.page.MainPage;
import lombok.RequiredArgsConstructor;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;

import javax.validation.constraints.NotBlank;

@RestController
@RequiredArgsConstructor
public class YoutubeShadowController {
    private final YoutubeRepository repository;
    private final DBService dbService;
    private final PlaylistsService playlistsService;


    @GetMapping(YoutubeEndpoints.getPlaylistContinue)
    public MainPage getPlaylistContinue(@NotBlank String token) throws Exception {
        return repository.getPlaylistAudios(token, false);
    }
}
