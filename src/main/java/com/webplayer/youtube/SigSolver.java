package com.webplayer.youtube;

import com.webplayer.objects.utils.DownloadUtil;
import lombok.RequiredArgsConstructor;
import org.apache.commons.lang3.StringUtils;

import javax.tools.JavaCompiler;
import javax.tools.ToolProvider;
import java.io.File;
import java.io.IOException;
import java.net.URL;
import java.net.URLClassLoader;
import java.nio.file.Files;
import java.util.function.UnaryOperator;

@RequiredArgsConstructor
public class SigSolver {

    public static String solve(String s, String playerUrl) throws Exception {
        return getInstance(playerUrl).apply(s);
    }

    private static String newDecipher(String playerUrl) throws IOException {
        String js = DownloadUtil.loadPageApache("http://youtube.com" + playerUrl);

        int rev = js.indexOf(":function(a){a.reverse()}");
        String reverse = js.substring(rev - 2, rev);
        System.out.println("reverse: " + reverse);

        int spl = js.indexOf(":function(a,b){a.splice(0,b)}");
        String splice = js.substring(spl - 2, spl);
        System.out.println("splice: " + splice);

        int sw = js.indexOf(":function(a,b){var c=a[0];a[0]=a[b%a.length];a[b%a.length]=c}");
        String swap = js.substring(sw - 2, sw);
        System.out.println("swap: " + swap);

        String[] allMatchingPatterns = StringUtils.substringsBetween(js, "{a=a.split(\"\");", ";return a.join(\"\")};");

        for (int i = allMatchingPatterns.length; i > 0; i--) {
            String mainFunc = allMatchingPatterns[i - 1];
            try {
                System.out.println("Sig function: " + mainFunc);

                StringBuilder code = new StringBuilder();
                code.append("import java.util.Arrays; import java.util.function.UnaryOperator; public class MyClass implements UnaryOperator<String> { public String apply(String s) {char temp; int ind; char[] a = s.toCharArray();");

                for (String command : mainFunc.split(";")) {
                    String[] parts = command.split("\\(");
                    String name = parts[0].substring(parts[0].length() - 2);
                    int param = Integer.parseInt(parts[1].substring(parts[1].indexOf(",") + 1, parts[1].length() - 1));

                    if (name.equals(reverse)) {
                        code.append("ind = a.length - 1;\n" +
                                "for (int i = 0; i < a.length / 2; i++, ind--) { temp = a[i];a[i] = a[ind];a[ind] = temp;}");
                    } else if (name.equals(splice)) {
                        code.append("a = Arrays.copyOfRange(a, ").append(param).append(", a.length);");
                    } else if (name.equals(swap)) {
                        code.append("ind = ").append(param).append(" % a.length; temp = a[0]; a[0] = a[ind]; a[ind] = temp;");
                    } else {
                        System.out.println("ERROR: SigSolver: parse js command name: " + name);
                        System.out.println(mainFunc);
                    }
                }

                code.append("return new String(a);}}");
                System.out.println("Success! Parsed!");
                return code.toString();
            } catch (Exception e) {
                System.out.println("Wrong function!");
            }
        }
        throw new IOException("Cant find/parse decipher function");
    }

    public static UnaryOperator<String> getInstance(String playerUrl) throws Exception {
        File root = new File("players/" + playerUrl.replaceAll("/", "_"));
        File classFile = new File(root, "MyClass.java");
        if (!classFile.exists()) {
            classFile.getParentFile().mkdirs();
            System.out.println("Creating \"" + classFile.getAbsolutePath() + "\"... " + (classFile.createNewFile() ? "Created!" : "Failed!")); // dont delete
            Files.writeString(classFile.toPath(), newDecipher(playerUrl));
            JavaCompiler compiler = ToolProvider.getSystemJavaCompiler();
            compiler.run(null, null, null, classFile.getPath());
        }

        URLClassLoader classLoader = URLClassLoader.newInstance(new URL[]{root.toURI().toURL()});
        Class<?> clazz = Class.forName("MyClass", true, classLoader);
        return (UnaryOperator<String>) clazz.newInstance();
    }

}
