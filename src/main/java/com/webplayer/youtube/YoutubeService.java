package com.webplayer.youtube;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.webplayer.database.User;
import com.webplayer.playlists.Playlist;
import com.webplayer.playlists.PlaylistsService;
import com.webplayer.unify.UService;
import com.webplayer.unify.formats.Format;
import com.webplayer.unify.page.MainPage;
import com.webplayer.unify.page.objects.Audio;
import com.webplayer.unify.params.search.YoutubeSearchParams;
import lombok.RequiredArgsConstructor;
import org.springframework.cache.annotation.Cacheable;
import org.springframework.stereotype.Service;

import java.util.Collections;
import java.util.List;
import java.util.Map;

import static com.webplayer.objects.main.configs.CacheConfig.YOUTUBE_LINK;

@Service
@RequiredArgsConstructor
public class YoutubeService implements UService {
    private final YoutubeRepository repository;
    private final PlaylistsService playlistsService;
    private final ObjectMapper mapper;

    @Override
    @Cacheable(value = YOUTUBE_LINK, key = "#id")
    public String getLink(User user, String id) throws Exception {
        return repository.getLink(id);
    }

    @Override
    public List<Format> getAllFormats(User user, String id) throws Exception {
        return repository.getFormats(id);
    }

    @Override
    public Audio getAudioById(User user, String id) throws Exception {
        return repository.getAudioById(id);
    }

    @Override
    public MainPage search(User user, Map<String, String> params) throws Exception {
        return repository.search(mapper.convertValue(params, YoutubeSearchParams.class));
    }

    @Override
    public List<Playlist> getMyPlaylists(User user) throws Exception {
        Playlist playlist = new Playlist("youtube", repository.musicChannelId, "Youtube Music");
        playlist.setThumbnail("https://sun9-16.userapi.com/c855732/v855732947/1e29a3/B1n340yjc2M.jpg");
        return Collections.singletonList(playlist);
    }

    @Override
    public MainPage getPlaylist(User user, String external_id) throws Exception {
        if (repository.musicChannelId.equals(external_id)) return repository.getMusicChannelPlaylists();
        if (external_id.startsWith("UC")) {
            return repository.getChannelAudios(external_id, true);
        } else {
            return repository.getPlaylistAudios(external_id, true);
        }
    }

    @Override
    public void addPlaylist(User user, String external_id, String title) throws Exception {
        Playlist playlist = new Playlist();
        playlist.setSource("youtube");
        playlist.setDynamic(true);
        playlist.setExternal_id(external_id);
        playlist.setTitle(title);
        playlist.setUser(user);
        playlistsService.addPlaylist(playlist);
    }
}
