package com.webplayer;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.cache.annotation.EnableCaching;

@SpringBootApplication
@EnableCaching
public class WebPlayerApplication {

	public static void main(String[] args) {
		SpringApplication.run(WebPlayerApplication.class, args);
	}

}
