package com.webplayer.objects.endpoints;

public class VkEndpoints {
    public static final String root = "/vk";

    public static final String playlist = root + Endpoints.getPlaylist;

    public static String getMyAudiosUrl() {
        return playlist;
    }
}
