package com.webplayer.objects.endpoints;

public class YoutubeEndpoints {
    public static final String root = "/youtube";
    public static final String getPlaylistContinue = root + "/playlist/continue";;

    public static String getPlaylistContinue(String token) {
        return token == null ? null :
                getPlaylistContinue + "?token=" + token;
    }

}
