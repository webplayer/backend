package com.webplayer.objects.endpoints;

public class PlaylistEndpoints {
    public static final String root = "/playlists";

    public static final String all = root + "/all";
    public static final String get = root + "/get";
    public static final String create = root + "/create";
    public static final String delete = root + "/delete";
    public static final String update = root + "/update";
    public static final String external = root + "/external";

    public static final String addTo = root + "/add";
    public static final String removeFrom = root + "/remove";


    public static String getPlaylist(Long id) {
        return root + get + "?id=" + id;
    }
}
