package com.webplayer.objects.endpoints;

import com.webplayer.unify.params.search.BasicSearchParams;

public class Endpoints {
    public static final String redirect = "/redirect";
    public static final String search = "/search";
    public static final String getPlaylist = "/playlists/get";

    public static String getSearchUrl(BasicSearchParams params) {
        return search + "?" + params.toRequestString();
    }
}
