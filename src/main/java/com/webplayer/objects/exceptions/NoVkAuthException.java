package com.webplayer.objects.exceptions;

import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.ResponseStatus;

@ResponseStatus(code = HttpStatus.FORBIDDEN, reason = "/vk/auth first")
public class NoVkAuthException extends Exception {
}

