package com.webplayer.objects.utils;

public class ParseUtils {
    public static int convertTimeString(String time, String delim) {
        int sum = 0;
        String[] arr = time.split(delim);
        for (int i = 1; i <= arr.length; i++) {
            int c = Integer.parseInt(arr[arr.length - i]);
            for (int j = 1; j < i; j++) c *= 60;
            sum += c;
        }
        return sum;
    }
}
