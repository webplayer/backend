package com.webplayer.objects.main.runners;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.webplayer.unify.params.search.VkSearchParams;
import com.webplayer.unify.params.search.YoutubeSearchParams;

import java.util.HashMap;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;

public class Test {

    public static void main(String[] args) throws Exception {
//        YoutubeRepository repository = new YoutubeRepository(null);

//        repository.getAudioById("R98w6sFQzig");

        VkSearchParams vkSearchParams = new VkSearchParams();
        YoutubeSearchParams youtubeSearchParams = new YoutubeSearchParams();

        List<Object> list = new LinkedList<>();
        list.add(vkSearchParams.getParams());
        list.add(youtubeSearchParams.getParams());

        Map<String, Object> map = new HashMap<>();
        map.put("search", list);

        System.out.println(new ObjectMapper().writeValueAsString(map));

        System.out.println(vkSearchParams.toRequestString());

        System.out.println(123);
    }
}
