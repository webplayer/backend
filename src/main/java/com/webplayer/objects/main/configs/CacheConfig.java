package com.webplayer.objects.main.configs;

import com.github.benmanes.caffeine.cache.Caffeine;
import org.springframework.cache.caffeine.CaffeineCache;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

import java.util.concurrent.TimeUnit;

@Configuration
public class CacheConfig {

    public final static String YOUTUBE_LINK = "youtubeLink";

    @Bean
    public CaffeineCache youtubeLink() {
        return new CaffeineCache(YOUTUBE_LINK,
                Caffeine.newBuilder()
                        .maximumSize(500)
                        .expireAfterWrite(3, TimeUnit.HOURS)
                        .build());
    }
    
}
