package com.webplayer.database;

import com.webplayer.playlists.Playlist;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;
import org.springframework.data.repository.query.Param;

import javax.transaction.Transactional;
import java.util.List;


public interface DbPlaylistRepository extends CrudRepository<Playlist, Long> {
    @Query("select new com.webplayer.playlists.Playlist(p.id, p.source, p.external_id, p.dynamic, p.title, p.thumbnail, p.songs) from Playlist p where p.user=:user order by p.id desc")
    List<Playlist> getUserPlaylists(@Param("user") User user);

    Playlist getByUserAndId(User user, Long id);

    @Transactional
    void deleteByUserAndId(User user, Long id);

    @Transactional
    @Modifying
    @Query(value = "update playlists_new p set songs = array_append(songs, :song_id\\:\\:text) where p.id = :id AND p.user_id = :user_id AND p.dynamic = false", nativeQuery = true)
    void pushToArray(@Param("user_id") Integer userId, @Param("id") Long id, @Param("song_id") String songId);

    @Transactional
    @Modifying
    @Query(value = "update playlists_new p set songs = array_remove(songs, :song_id\\:\\:text) where p.id = :id AND p.user_id = :user_id AND p.dynamic = false", nativeQuery = true)
    void removeFromArray(@Param("user_id") Integer userId, @Param("id") Long id, @Param("song_id") String songId);
}
