package com.webplayer.database;

import org.springframework.data.repository.CrudRepository;

import java.util.Optional;

public interface DBYoutubePlayerRepository extends CrudRepository<Player, String> {
    Optional<Player> getByPath(String path);
}
