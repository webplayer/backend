package com.webplayer.database;

import com.webplayer.unify.page.objects.Audio;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;
import org.springframework.data.repository.query.Param;

import java.util.List;

public interface DBCacheRepository extends CrudRepository<Audio, String> {
    @Query(value = "select c.* from (select unnest(songs) as s_id from public.playlists_new p where p.id=:id and user_id=:user_id) u join cache c on c.id = u.s_id", nativeQuery = true)
    List<Audio> getPlaylistAudios(@Param("user_id") Integer userId, @Param("id") Long id);
}
