package com.webplayer.database;

import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

import java.util.Optional;

@Repository
public interface DBAuthRepository extends CrudRepository<User, Integer> {
    Optional<User> findByLoginAndPasswordHash(String login, String passwordHash);
    Optional<User> findByToken(String token);
    boolean existsByToken(String token);
}

