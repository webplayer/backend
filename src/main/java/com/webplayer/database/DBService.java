package com.webplayer.database;

import com.webplayer.objects.exceptions.BadAuthException;
import com.webplayer.objects.exceptions.NoVkAuthException;
import com.webplayer.vk.VkAuthInfo;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;

import java.util.Optional;

@Service
@RequiredArgsConstructor
public class DBService {

    private final DBAuthRepository dbAuthRepository;

    public User getUser(String login, String password) {
        return dbAuthRepository.findByLoginAndPasswordHash(login, password).orElse(null);
    }

    public User getUser(String token) throws Exception {
        Optional<User> opt = dbAuthRepository.findByToken(token);
        if (opt.isEmpty()) throw new BadAuthException();
        return opt.get();
    }

    public User getUserNoException(String token) {
        return dbAuthRepository.findByToken(token).orElse(null);
    }

    public User checkVkAuth(String token) throws Exception {
        User user = getUser(token);
        if (user.getVkAuthInfo() == null) throw new NoVkAuthException();
        return user;
    }

    public VkAuthInfo getVkAuth(String token) throws Exception {
        return checkVkAuth(token).getVkAuthInfo();
    }

    public User checkVkNoExcept(String token) {
        Optional<User> opt = dbAuthRepository.findByToken(token);
        if (opt.isEmpty()) return null;
        User user = opt.get();
        if (user.getVkAuthInfo() == null) return null;
        return user;
    }

    public int checkVkUser(User user) {
        if (user == null) {
            return 401;
        } else if (user.getVkAuthInfo() == null) {
            return 403;
        }
        return 200;
    }
}
