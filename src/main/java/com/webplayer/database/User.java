package com.webplayer.database;

import com.webplayer.vk.VkAuthInfo;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.persistence.*;

@Data
@NoArgsConstructor
@Entity
@Table(name = "users")
public class User {

    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private Integer id;

    @Column(unique = true, nullable = false)
    private String login;

    @Column(nullable = false)
    private String passwordHash;

    private String token;

    @OneToOne(cascade = CascadeType.ALL)
    @JoinColumn(name = "vk_info_id")
    private VkAuthInfo vkAuthInfo;
}
