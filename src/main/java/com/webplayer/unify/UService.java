package com.webplayer.unify;

import com.webplayer.database.User;
import com.webplayer.objects.exceptions.NotPresented;
import com.webplayer.playlists.Playlist;
import com.webplayer.unify.formats.Format;
import com.webplayer.unify.page.MainPage;
import com.webplayer.unify.page.objects.Audio;
import com.webplayer.unify.params.auth.AuthParams;

import java.util.List;
import java.util.Map;

public interface UService {

    default boolean auth(User user, AuthParams params) throws Exception {
        return true;
    }

    default boolean checkAuth(User user) throws Exception  {
        return true;
    }

    default String getLink(User user, String id) throws Exception {
        throw new NotPresented();
    }

    default List<Format> getAllFormats(User user, String id) throws Exception {
        throw new NotPresented();
    }

    default Audio getAudioById(User user, String id) throws Exception {
        throw new NotPresented();
    }

    default MainPage search(User user, Map<String, String> params) throws Exception {
        throw new NotPresented();
    }

    default List<Playlist> getMyPlaylists(User user) throws Exception {
        return null;
    }

    default MainPage getPlaylist(User user, String external_id) throws Exception {
        throw new NotPresented();
    }

    default void addPlaylist(User user, String external_id, String title) throws Exception {
        throw new NotPresented();
    }
}
