package com.webplayer.unify;

import com.webplayer.database.DBService;
import com.webplayer.database.User;
import com.webplayer.objects.endpoints.Endpoints;
import com.webplayer.unify.formats.Format;
import com.webplayer.unify.page.MainPage;
import com.webplayer.unify.page.objects.Audio;
import com.webplayer.unify.params.auth.AuthParams;
import com.webplayer.unify.params.auth.AuthResponse;
import lombok.RequiredArgsConstructor;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.util.List;
import java.util.Map;

@RestController
@RequiredArgsConstructor
public class UController {
    private final ServiceSelector serviceSelector;
    private final DBService dbService;

    @PostMapping("/auth")
    public boolean login(@CookieValue String token, @RequestParam String s, @Valid AuthParams params) throws Exception {
        User user = dbService.getUser(token);
        return serviceSelector.get(s).auth(user, params);
    }

    @GetMapping("/auth")
    public boolean checkAuth(@CookieValue(required = false) String token, @RequestParam String s) throws Exception {
        User user = dbService.getUserNoException(token);
        return serviceSelector.get(s).checkAuth(user);
    }

    @GetMapping("/status")
    public AuthResponse getAuthStatus(@CookieValue String token) throws Exception {
        User user = dbService.getUser(token);
        AuthResponse response = new AuthResponse();
        response.setUsername(user.getLogin());
        for (Map.Entry<String, UService> entry : serviceSelector.entrySet()) {
            response.getStatus().put(entry.getKey(), entry.getValue().checkAuth(user));
        }
        return response;
    }

    @GetMapping("/link")
    public String link(@CookieValue(required = false) String token, @RequestParam String s, @RequestParam String id) throws Exception {
        User user = dbService.getUserNoException(token);
        return serviceSelector.get(s).getLink(user, id);
    }

    @GetMapping("/formats")
    public List<Format> formats(@CookieValue(required = false) String token, @RequestParam String s, @RequestParam String id) throws Exception {
        User user = dbService.getUserNoException(token);
        return serviceSelector.get(s).getAllFormats(user, id);
    }

    @GetMapping("/redirect")
    public ResponseEntity redirectLink(@CookieValue(required = false) String token, @RequestParam String s, @RequestParam String id) throws Exception {
        User user = dbService.getUserNoException(token);
        HttpHeaders headers = new HttpHeaders();
        headers.add("Location", serviceSelector.get(s).getLink(user, id));
        return new ResponseEntity<byte[]>(null, headers, HttpStatus.FOUND);
    }

    @GetMapping("/audio")
    public Audio getAudioById(@CookieValue(required = false) String token, @RequestParam String s, @RequestParam String id) throws Exception {
        User user = dbService.getUserNoException(token);
        return serviceSelector.get(s).getAudioById(user, id);
    }

    @GetMapping(Endpoints.search)
    public MainPage searchAudios(@CookieValue(required = false) String token, @RequestParam String s, @RequestParam Map<String, String> searchParams) throws Exception {
        System.out.println(s + searchParams);
        return serviceSelector.get(s).search(dbService.getUserNoException(token), searchParams);
    }

}
