package com.webplayer.unify.page.objects;

import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@NoArgsConstructor
@AllArgsConstructor
public class Correction {
    @JsonProperty("old")
    String oldQ;
    @JsonProperty("new")
    String newQ;
}
