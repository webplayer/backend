package com.webplayer.unify.page.objects;

import com.webplayer.youtube.parsers.VideoParser;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;

@Data
@NoArgsConstructor
@AllArgsConstructor
@Entity
@Table(name = "cache")
public class Audio {

    String source;

    @Id
    String id;

    String title;
    String artist;

    Integer length;

    @Column(columnDefinition="TEXT")
    String thumbnail;

    public Audio(VideoParser parser) {
        this.source = "youtube";
        this.id = parser.getId();
        this.title = parser.getTitle();
        this.artist = parser.getArtist();
        this.length = parser.getLength();
        this.thumbnail = parser.getThumbnail();
    }

}
