package com.webplayer.unify.page.objects;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.webplayer.unify.page.containers.BaseType;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.util.LinkedList;
import java.util.List;

@Data
@NoArgsConstructor
@AllArgsConstructor
@JsonInclude(JsonInclude.Include.NON_NULL)
public class PlaylistInfo {
    String title;
    String thumbnail;
    List<BaseType> playlists;
}
