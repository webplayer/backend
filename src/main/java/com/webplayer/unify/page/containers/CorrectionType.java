package com.webplayer.unify.page.containers;

import com.webplayer.unify.page.objects.Correction;
import lombok.AllArgsConstructor;
import lombok.Data;

@Data
@AllArgsConstructor
public class CorrectionType implements BaseType {
    Correction correction;
}
