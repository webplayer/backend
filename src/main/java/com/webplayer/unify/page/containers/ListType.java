package com.webplayer.unify.page.containers;

import com.webplayer.unify.page.MainPage;
import lombok.AllArgsConstructor;
import lombok.Data;

@Data
@AllArgsConstructor
public class ListType implements BaseType {
    MainPage list;
}
