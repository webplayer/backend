package com.webplayer.unify.page.containers;

import com.webplayer.playlists.Playlist;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@NoArgsConstructor
@AllArgsConstructor
public class PlaylistType implements BaseType {
    Playlist playlist;
}
