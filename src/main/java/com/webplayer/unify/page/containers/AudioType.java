package com.webplayer.unify.page.containers;

import com.webplayer.unify.page.objects.Audio;
import lombok.Data;

@Data
public class AudioType implements BaseType {
    Audio audio;

    public AudioType(Audio audio) {
        this.audio = audio;
    }
}
