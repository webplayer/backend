package com.webplayer.unify.page.containers;

import com.webplayer.playlists.Playlist;
import com.webplayer.unify.page.MainPage;
import com.webplayer.unify.page.objects.Audio;
import com.webplayer.unify.page.objects.Correction;

public class TypeFactory {
    public static AudioType get(Audio audio) {
        return new AudioType(audio);
    }

    public static PlaylistType get(Playlist playlist) {
        return new PlaylistType(playlist);
    }

    public static CorrectionType get(Correction correction) {
        return new CorrectionType(correction);
    }

    public static ListType get(MainPage page) {
        return new ListType(page);
    }

}
