package com.webplayer.unify.page;

import com.fasterxml.jackson.annotation.JsonAnyGetter;
import com.fasterxml.jackson.annotation.JsonInclude;
import com.webplayer.unify.page.containers.BaseType;
import com.webplayer.unify.page.objects.PlaylistInfo;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.util.HashMap;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;

@Data
@NoArgsConstructor
@JsonInclude(JsonInclude.Include.NON_NULL)
public class MainPage {
    String next;

    Map<String, Object> map = new HashMap<>();

    PlaylistInfo info;

    List<BaseType> data = new LinkedList<>();

    public MainPage(String next, List<BaseType> data) {
        this.next = next;
        this.data = data;
    }

    @JsonAnyGetter
    public Map<String, Object> getMap() {
        return map;
    }
}
