package com.webplayer.unify.formats;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@NoArgsConstructor
@AllArgsConstructor
public class Format {
    String mime;
    String quality;
    String url;
}
