package com.webplayer.unify.params.annotations;

import com.fasterxml.jackson.annotation.JsonProperty;

import java.lang.annotation.*;

@Retention(RetentionPolicy.RUNTIME)
@Target(ElementType.FIELD)
@Repeatable(Values.class)
public @interface Value {
    @JsonProperty
    String name();
    @JsonProperty
    String value();
}
