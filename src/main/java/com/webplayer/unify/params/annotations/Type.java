package com.webplayer.unify.params.annotations;

public enum Type {
    DEFAULT,
    SWITCH,
    SELECT,
    SEARCH,
    TEXT
}
