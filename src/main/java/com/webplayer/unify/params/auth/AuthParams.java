package com.webplayer.unify.params.auth;

import lombok.Data;
import lombok.NoArgsConstructor;

import javax.validation.constraints.NotBlank;

@Data
@NoArgsConstructor
public class AuthParams {
    @NotBlank
    public String username;

    @NotBlank
    public String password;

    public boolean force = false;
}
