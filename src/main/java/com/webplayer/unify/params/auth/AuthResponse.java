package com.webplayer.unify.params.auth;

import lombok.Data;
import lombok.NoArgsConstructor;

import java.util.HashMap;
import java.util.Map;

@Data
@NoArgsConstructor
public class AuthResponse {
    String username;
    Map<String, Boolean> status = new HashMap<>();
}
