package com.webplayer.unify.params.search;

import com.webplayer.unify.params.annotations.Name;
import com.webplayer.unify.params.annotations.Prefix;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@Getter
@Setter
@NoArgsConstructor
@Prefix(value = "youtube", color = "red")
public class YoutubeSearchParams extends BasicSearchParams {
    @Name("Автоисправление")
    public boolean correction = true;

    @Name("Только аудио")
    public boolean videos = false;

    public String token;
}
