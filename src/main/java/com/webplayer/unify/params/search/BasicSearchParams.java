package com.webplayer.unify.params.search;

import com.webplayer.unify.params.annotations.Name;
import com.webplayer.unify.params.annotations.Prefix;
import com.webplayer.unify.params.annotations.Type;
import com.webplayer.unify.params.annotations.Values;
import lombok.Data;

import javax.validation.constraints.NotBlank;
import java.lang.reflect.Field;
import java.net.URLEncoder;
import java.nio.charset.StandardCharsets;
import java.util.HashMap;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;

@Data
public class BasicSearchParams {
    @NotBlank
    @Name(type = Type.SEARCH)
    public String q;

    public Map<String, Object> getParams() throws Exception {
        Map<String, Object> map = new HashMap<>();
        List<Map<String, Object>> list = new LinkedList<>();

        Class clazz = this.getClass();

        Prefix pAnnotation = (Prefix) clazz.getAnnotation(Prefix.class);
//        String prefix = pAnnotation.value() + ".";

        map.put("source", pAnnotation.value());
        map.put("color", pAnnotation.color());
        map.put("params", list);

        Field[] allFields = clazz.getFields();

        for (Field f : allFields) {
            Name name = f.getAnnotation(Name.class);
            if (name == null) continue;
            Map<String, Object> pm = new HashMap<>();
            pm.put("param", f.getName());
            Object defValue = f.get(this);
            if (defValue != null) pm.put("default", defValue);
            if (name.value().length() > 0) pm.put("name", name.value());
            if (name.tooltip().length() > 0) pm.put("tooltip", name.tooltip());

            Type type = name.type();
            if (type == Type.DEFAULT) {
                if (f.getType() == boolean.class) {
                    type = Type.SWITCH;
                } else if (f.getType() == String.class) {
                    type = Type.TEXT;
                } else {
                    type = Type.SELECT;
                }
            }

            switch (type) {
                case SWITCH: {
                    pm.put("type", "switch");
                } break;

                case SELECT: {
                    pm.put("type", "select");
                    pm.put("values", f.getAnnotation(Values.class).value());
                } break;

                case SEARCH: {
                    pm.put("type", "search");
                } break;

                default:
                    continue;
            }

            list.add(pm);
        }

        return map;
    }

    public String toRequestString() {
        StringBuilder builder = new StringBuilder();
        for (Field f : this.getClass().getFields()) {
            try {
                f.setAccessible(true);
                if (f.get(this) == null) continue;
                builder.append(f.getName()).append('=').append(URLEncoder.encode(f.get(this).toString(), StandardCharsets.UTF_8)).append('&');
            } catch (IllegalAccessException e) {
                e.printStackTrace();
            }
        }
        return builder.deleteCharAt(builder.length() - 1).toString();
    }
}
