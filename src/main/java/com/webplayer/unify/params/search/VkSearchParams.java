package com.webplayer.unify.params.search;

import com.webplayer.unify.params.annotations.Name;
import com.webplayer.unify.params.annotations.Prefix;
import com.webplayer.unify.params.annotations.Value;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@Getter
@Setter
@NoArgsConstructor
@Prefix(value = "vk", color = "blue")
public class VkSearchParams extends BasicSearchParams {

    @Name("Поиск по своим")
    public boolean own = false;

    @Name("Поиск по артисту")
    public boolean artist = false;

    @Name("Автокоррекция")
    public boolean autocomplete = false;
    
    @Name("Сортировка")
    @Value(name = "по популярности", value = "2")
    @Value(name = "по длительности", value = "1")
    @Value(name = "по дате добавления", value = "0")
    public int sort = 2;

    public int page = 1;

}
