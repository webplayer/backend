package com.webplayer.unify;

import com.webplayer.vk.VkService;
import com.webplayer.youtube.YoutubeService;
import org.springframework.stereotype.Service;

import java.util.HashMap;
import java.util.Map;
import java.util.Set;

@Service
public class ServiceSelector {
    private final Map<String, UService> serviceMap = new HashMap<>();

    public ServiceSelector(VkService vkService, YoutubeService youtubeService) {
        serviceMap.put("vk", vkService);
        serviceMap.put("youtube", youtubeService);
    }

    public UService get(String s) {
        return serviceMap.get(s);
    }

    public Set<Map.Entry<String, UService>> entrySet() {
        return serviceMap.entrySet();
    }
}
