package com.webplayer.controllers;

import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.webplayer.unify.params.search.VkSearchParams;
import com.webplayer.unify.params.search.YoutubeSearchParams;
import lombok.RequiredArgsConstructor;
import org.apache.http.HttpResponse;
import org.apache.http.client.HttpClient;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.impl.client.HttpClients;
import org.apache.http.util.EntityUtils;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;

import javax.validation.constraints.NotBlank;
import java.net.URLEncoder;
import java.nio.charset.StandardCharsets;
import java.util.HashMap;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;

@RestController
@RequiredArgsConstructor
public class MainController {
    private final HttpClient httpClient = HttpClients.createDefault();
    private String mainMapping;


    @GetMapping("/map")
    public String map() throws Exception {
        if (mainMapping != null) return mainMapping;

        List<Object> list = new LinkedList<>();
        list.add(new YoutubeSearchParams().getParams());
        list.add(new VkSearchParams().getParams());

        Map<String, Object> map = new HashMap<>();
        map.put("search", list);

        Map<String, Object> root = new HashMap<>();
        root.put("endpoints", map);

        mainMapping = new ObjectMapper().writeValueAsString(root);
        return mainMapping;
    }

    @GetMapping("/autocomplete")
    public List<String> autoComplete(@NotBlank String q) {
        try {
            HttpGet request = new HttpGet(
                    "https://suggestqueries.google.com/complete/search?&ds=yt&client=youtube&q="
                            + URLEncoder.encode(q, StandardCharsets.UTF_8));
            HttpResponse response = httpClient.execute(request);

            String str = EntityUtils.toString(response.getEntity());
            JsonNode node = new ObjectMapper().readTree(str.substring(str.indexOf('(') + 1, str.length() - 1));
            List<String> res = new LinkedList<>();

            for (JsonNode elem : node.get(1)) {
                res.add(elem.get(0).asText());
            }

            return res;
        } catch (Exception e) {
            return new LinkedList<>();
        }
    }

}
