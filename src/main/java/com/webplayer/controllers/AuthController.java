package com.webplayer.controllers;

import com.webplayer.database.DBAuthRepository;
import com.webplayer.database.DBService;
import com.webplayer.database.User;
import com.webplayer.objects.exceptions.BadUsernamePasswordException;
import lombok.RequiredArgsConstructor;
import org.springframework.util.DigestUtils;
import org.springframework.web.bind.annotation.*;

import javax.servlet.http.Cookie;
import javax.servlet.http.HttpServletResponse;
import java.util.Random;

@RestController
@RequiredArgsConstructor
public class AuthController {

    private final DBAuthRepository dbAuthRepository;
    private final DBService dbService;

    @PostMapping("/register")
    public void register(HttpServletResponse response, @RequestParam String username, @RequestParam String password) {
        try {
            User user = new User();

            user.setLogin(username);
            user.setPasswordHash(DigestUtils.md5DigestAsHex((username + password).getBytes()));

            String token;
            do {
                String preToken = username + (new Random()).nextInt() + System.currentTimeMillis();
                token = DigestUtils.md5DigestAsHex(preToken.getBytes());
            } while (dbAuthRepository.existsByToken(token));

            user.setToken(token);
            dbAuthRepository.save(user);

            Cookie cookie = new Cookie("token",  user.getToken());
            cookie.setMaxAge(31536000);

            response.addCookie(cookie);
        } catch (Exception e) {
            response.setStatus(401);
        }
    }

    @GetMapping("/login")
    public void login(HttpServletResponse response, @RequestParam String username, @RequestParam String password) throws BadUsernamePasswordException {
        User user = dbService.getUser(username, DigestUtils.md5DigestAsHex((username + password).getBytes()));
        if (user == null) {
            throw new BadUsernamePasswordException();
        }
        Cookie cookie = new Cookie("token",  user.getToken());
        cookie.setMaxAge(31536000);
        
        response.addCookie(cookie);
    }

    @DeleteMapping("/login")
    public void logout(HttpServletResponse response) {
        Cookie cookie = new Cookie("token", null);
        cookie.setMaxAge(0);
        response.addCookie(cookie);
    }

    @GetMapping("/user")
    public String getUsername(@CookieValue String token) throws Exception {
        return dbService.getUser(token).getLogin();
    }

}
