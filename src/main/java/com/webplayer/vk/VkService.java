package com.webplayer.vk;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.webplayer.database.DBAuthRepository;
import com.webplayer.database.User;
import com.webplayer.playlists.PlaylistsService;
import com.webplayer.playlists.Playlist;
import com.webplayer.unify.UService;
import com.webplayer.unify.formats.Format;
import com.webplayer.unify.page.MainPage;
import com.webplayer.unify.page.objects.Audio;
import com.webplayer.unify.params.auth.AuthParams;
import com.webplayer.unify.params.search.VkSearchParams;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;

import java.util.Collections;
import java.util.List;
import java.util.Map;

@Service
@RequiredArgsConstructor
public class VkService implements UService {
    private final DBAuthRepository dbAuthRepository;
    private final PlaylistsService playlistsService;
    private final VK_Repository repository;
    private final ObjectMapper mapper;

    @Override
    public boolean auth(User user, AuthParams params) throws Exception {
        if ((user.getVkAuthInfo() != null) && !params.force) {
            return true; //already have token
        }
        try {
            VkAuthInfo authInfo = repository.getToken(params.username, params.password);
            user.setVkAuthInfo(authInfo);
            dbAuthRepository.save(user);
            return true;
        } catch (Exception e) {
            e.printStackTrace();
            return false;
        }
    }

    @Override
    public boolean checkAuth(User user) throws Exception {
        return (user != null) && (user.getVkAuthInfo() != null);
    }

    @Override
    public String getLink(User user, String id) throws Exception {
        return repository.getLink(user.getVkAuthInfo(), id);
    }

    @Override
    public List<Format> getAllFormats(User user, String id) throws Exception {
        return Collections.singletonList(new Format("audio/mpeg", "300000", getLink(user, id)));
    }

    @Override
    public Audio getAudioById(User user, String id) throws Exception {
        return repository.getAudio(user.getVkAuthInfo(), id);
    }

    @Override
    public MainPage search(User user, Map<String, String> params) throws Exception {
        return repository.search(user.getVkAuthInfo(), mapper.convertValue(params, VkSearchParams.class));
    }

    @Override
    public List<Playlist> getMyPlaylists(User user) throws Exception {
        if (user.getVkAuthInfo() == null) return null;
        Playlist playlist = new Playlist("vk", user.getVkAuthInfo().getMyId(), true, "VK музыка", null);
        playlist.setThumbnail("https://sun9-1.userapi.com/c855732/v855732947/1e2aab/-Q5Oj1ru1nQ.jpg");
        return Collections.singletonList(playlist);
    }

    @Override
    public MainPage getPlaylist(User user, String external_id) throws Exception {
        String owner, album;
        if (external_id.contains("_")) {
            String[] parts = external_id.split("_");
            owner = parts[0];
            album = parts[1];
        } else {
            owner = external_id;
            album = null;
        }
        return new MainPage(null, repository.getAudiosByOwnerAndAlbum(user.getVkAuthInfo(), owner, album));
    }

    @Override
    public void addPlaylist(User user, String external_id, String title) throws Exception {
        Playlist playlist = new Playlist();
        playlist.setSource("vk");
        playlist.setDynamic(true);
        playlist.setExternal_id(external_id);
        playlist.setTitle(title);
        playlist.setUser(user);
        playlistsService.addPlaylist(playlist);
    }
}
