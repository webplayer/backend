package com.webplayer.vk;

import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.webplayer.unify.params.search.VkSearchParams;

import java.io.BufferedReader;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.URL;

public class VK_API {

    static VkRequest getAudioById(VkAuthInfo authInfo, String id) throws Exception {
        return new VkRequest("audio.getById", authInfo)
                .param("audios", id)
                .run();
    }

    static VkRequest search(VkAuthInfo authInfo, VkSearchParams params) throws Exception {
        return new VkRequest("audio.search", authInfo)
                .param("q", params.q)
                .param("count", 20)
                .param("offset", (params.page - 1) * 20)
                .param("search_own", params.own ? "1" : "0")
                .param("performer_only", params.artist ? "1" : "0")
                .param("auto_complete", params.autocomplete ? "1" : "0")
                .param("sort", params.sort)
                .run();
    }

    static JsonNode getToken(String username, String password) throws Exception {
        HttpURLConnection con = (HttpURLConnection) new URL(
                "https://oauth.vk.com/token?grant_type=password" +
                        "&client_id=2274003" +
                        "&scope=audio,offline,nohttps" +
                        "&client_secret=hHbZxrka2uZ6jB1inYsH" +
                        "&username=" + username +
                        "&password=" + password).openConnection();
        con.setRequestMethod("GET");
        con.setRequestProperty("User-Agent", "VKAndroidApp/5.23-2978 (Android 4.4.2; SDK 19; x86; unknown Android SDK built for x86; en; 320x240)");

        try (InputStreamReader is = new InputStreamReader(con.getInputStream());
             BufferedReader reader = new BufferedReader(is)) {
            return new ObjectMapper().readTree(reader);
        }
    }

    static VkRequest refreshToken(VkAuthInfo authInfo) throws Exception {
        String device_id = VkUtils.generateDeviceId();

        JsonNode ex = new VkRequest("execute.getUserInfo", authInfo)
                .param("https", 1)
                .param("androidVersion", 19)
                .param("androidModel", "Android SDK built for x86")
                .param("info_fields", "audio_ads,audio_background_limit,country,discover_design_version,discover_preload,discover_preload_not_seen,gif_autoplay,https_required,inline_comments,intro,lang,menu_intro,money_clubs_p2p,money_p2p,money_p2p_params,music_intro,audio_restrictions,profiler_settings,raise_to_record_enabled,stories,masks,subscriptions,support_url,video_autoplay,video_player,vklive_app,community_comments,webview_authorization,story_replies,animated_stickers,community_stories,live_section,playlists_download,calls,security_issue,eu_user,wallet,vkui_community_create,vkui_profile_edit,vkui_community_manage,vk_apps,stories_photo_duration,stories_reposts,live_streaming,live_masks,camera_pingpong,role,video_discover")
                .param("device_id", device_id)
                .param("lang", "en")
                .param("func_v", 11)
                .param("androidManufacturer", "unknown")
                .param("fields", "photo_100,photo_50,exports,country,sex,status,bdate,first_name_gen,last_name_gen,verified,trending").run().getResponse();

        return new VkRequest("auth.refreshToken", authInfo)
                .param("https", 1)
                .param("timestamp", 0)
                .param("receipt2", "")
                .param("device_id", device_id)
                .param("receipt", "")
                .param("lang", "en")
                .param("nonce", "").run();
    }
}
