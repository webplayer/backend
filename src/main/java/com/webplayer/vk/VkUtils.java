package com.webplayer.vk;

import com.fasterxml.jackson.databind.JsonNode;
import com.webplayer.unify.page.objects.Audio;

import java.util.Random;

public class VkUtils {
    private static final char[] deviceChars = {'1', '2', '3', '4', '5', '6', '7', '8', '9', '0', 'a', 'b', 'c', 'd', 'e', 'f'};

    static Audio parseAudio(JsonNode item) {
        return new Audio("vk",
                item.get("owner_id").asText() + '_' + item.get("id").asText(),
                item.get("title").asText(),
                item.get("artist").asText(),
                item.get("duration").asInt(),
                item.hasNonNull("album") ? item.get("album").get("thumb").get("photo_600").asText() : null);
    }

    static String getUrl(JsonNode item) {
        String url = item.get("url").asText();

        int first_marker = url.indexOf('/', url.indexOf('/', 11) + 1) + 1;
        int second_marker;
        int third_marker;

        if (url.substring(8, 12).equals("psv4")) {
            first_marker = url.indexOf('/', first_marker) + 1;
            second_marker = url.indexOf('/', first_marker) + 1;
            third_marker = url.indexOf('/',url.indexOf('/', second_marker) + 1);
        } else {
            second_marker = url.indexOf('/', first_marker) + 1;
            third_marker = url.indexOf('/', second_marker);
        }

        return url.substring(0, first_marker) +
                url.substring(second_marker, third_marker) + ".mp3" +
                url.substring(url.indexOf('?', third_marker));
    }

    static String generateDeviceId() {
        char[] dev = new char[16];
        Random rand = new Random();
        for (int i = 0; i < 16; i++) {
            dev[i] = deviceChars[rand.nextInt(16)];
        }
        return String.valueOf(dev);
    }
}
