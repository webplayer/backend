package com.webplayer.vk;

import com.fasterxml.jackson.databind.JsonNode;
import com.webplayer.objects.endpoints.Endpoints;
import com.webplayer.unify.params.search.VkSearchParams;
import com.webplayer.unify.page.MainPage;
import com.webplayer.unify.page.containers.AudioType;
import com.webplayer.unify.page.containers.BaseType;
import com.webplayer.unify.page.containers.TypeFactory;
import com.webplayer.unify.page.objects.Audio;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Repository;

import java.util.LinkedList;
import java.util.List;

@Repository
@RequiredArgsConstructor
public class VK_Repository {

    public String getLink(VkAuthInfo authInfo, String id) throws Exception {
        JsonNode node = VK_API.getAudioById(authInfo, id).getResponse();

        return VkUtils.getUrl(node.get(0));
    }

    public Audio getAudio(VkAuthInfo authInfo, String id) throws Exception {
        JsonNode node = VK_API.getAudioById(authInfo, id).getResponse();

        return VkUtils.parseAudio(node.get(0));
    }

    public MainPage search(VkAuthInfo authInfo, VkSearchParams params) throws Exception {
        MainPage page = new MainPage();
        JsonNode node = VK_API.search(authInfo, params).getResponse().get("items");

        List<BaseType> list = page.getData();
        for (JsonNode item : node) {
            try {
                list.add(new AudioType(VkUtils.parseAudio(item)));
            } catch (Exception ignored) {
            }
        }

        params.page++;
        page.setNext(list.size() == 0 ? null : Endpoints.search + "?" + "s=vk&" + params.toRequestString());
        return page;
    }

    public List<BaseType> getAudiosByOwnerAndAlbum(VkAuthInfo authInfo, String owner_id, String album_id) throws Exception {
        VkRequest request = new VkRequest("audio.get", authInfo)
                .param("count", 6000);
        if (owner_id != null) request.param("owner_id", owner_id);
        if (album_id != null) request.param("album_id", album_id);

        request.param("need_user", 1);

        JsonNode node = request.run().getResponse().get("items");

        List<BaseType> list = new LinkedList<>();
        for (JsonNode item : node) {
            try {
                list.add(TypeFactory.get(VkUtils.parseAudio(item)));
            } catch (Exception ignored) {
            }
        }
        return list;
    }

    public VkAuthInfo getToken(String username, String password) throws Exception {
        JsonNode response = VK_API.getToken(username, password);
        VkAuthInfo authInfo = new VkAuthInfo(response.get("access_token").asText(), response.get("secret").asText(), response.get("user_id").asText());
        VK_API.refreshToken(authInfo);
        return authInfo;
    }

}
