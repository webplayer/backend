package com.webplayer.vk;

import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.ObjectMapper;
import org.springframework.util.DigestUtils;

import java.io.BufferedReader;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.URL;
import java.net.URLEncoder;
import java.nio.charset.StandardCharsets;
import java.util.LinkedHashMap;
import java.util.Map;
import java.util.Random;

public class VkRequest {
    private static final ObjectMapper mapper = new ObjectMapper();
    private static final String API_URL = "https://api.vk.com/method/";
    private LinkedHashMap<String, String> params = new LinkedHashMap<>();
    private String method;
    private String secret;

    private boolean hasError = false;
    private JsonNode response;
    private int tries = 0;


    public VkRequest(String method, String token, String secret) {
        this.method = method;
        this.secret = secret;
        params.put("v", "5.93");
        params.put("access_token", token);
    }

    public VkRequest(String method, VkAuthInfo info) {
        this.method = method;
        this.secret = info.getVkSecret();
        params.put("v", "5.93");
        params.put("access_token", info.getVkToken());
    }

    public VkRequest param(String name, Object value) {
        params.put(name, (value == null? "" : value.toString()));
        return this;
    }

    public boolean hasErrors() {
        return hasError;
    }

    private String getSig() {
        StringBuilder sb = new StringBuilder("/method/").append(method).append("?");
        for(Map.Entry<String, String> e : params.entrySet()) {
            sb.append(e.getKey()).append("=").append(e.getValue()).append("&");
        }
        return DigestUtils.md5DigestAsHex((sb.toString().substring(0, sb.length() - 1) + secret).getBytes());
    }

    public JsonNode getResponse() {
        return response;
    }

    public VkRequest run() throws Exception{
        StringBuilder sb = new StringBuilder(API_URL).append(method).append("?");

        for(Map.Entry<String, String> e : params.entrySet()) {
            sb.append(e.getKey()).append("=").append(URLEncoder.encode(e.getValue(), StandardCharsets.UTF_8).replace("+", "%20")).append("&");
        }
        sb.append("sig").append("=").append(getSig());
        String url = sb.toString();

        tries = 0;

        do {
            tries ++;
            HttpURLConnection con = (HttpURLConnection) new URL(url).openConnection();
            con.setRequestMethod("GET");
            con.setRequestProperty("User-Agent", "VKAndroidApp/5.23-2978 (Android 4.4.2; SDK 19; x86; unknown Android SDK built for x86; en; 320x240)");

            try (InputStreamReader is = new InputStreamReader(con.getInputStream());
                 BufferedReader reader = new BufferedReader(is)) {
                response = mapper.readTree(reader);
            }
        } while (checkErrors());
        return this;
    }

    private boolean checkErrors() throws InterruptedException {
        if (response.has("response")) {
            response = response.get("response");
            hasError = false;
            return false;
        } else if (response.has("error")) {
            response = response.get("error");
            hasError = true;
            int code = response.get("error_code").asInt();

            switch (code) {
                case 1:   // Произошла неизвестная ошибка
                case 6:   // Слишком много запросов в секунду
                case 10:  // Произошла внутренняя ошибка сервера
                case 14:  // Требуется ввод кода с картинки (Captcha)

                    if (tries <= 3) {
                        Thread.sleep(new Random().nextInt(200) + 400);  // 400-600ms sleep
                        return true;
                    }
                    return false;
            }
        }

        return false;
    }
}
