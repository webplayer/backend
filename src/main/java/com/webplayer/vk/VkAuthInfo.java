package com.webplayer.vk;

import lombok.Data;
import lombok.NoArgsConstructor;

import javax.persistence.*;

@Data
@NoArgsConstructor
@Entity
@Table(name = "vk_info")
public class VkAuthInfo {
    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private Integer id;

    public VkAuthInfo(String vkToken, String vkSecret, String myId) {
        this.vkToken = vkToken;
        this.vkSecret = vkSecret;
        this.myId = myId;
    }

    private String vkToken;
    private String vkSecret;
    private String myId;
}
