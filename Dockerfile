FROM openjdk:12-alpine
RUN apk add tzdata
RUN cp /usr/share/zoneinfo/Europe/Moscow /etc/localtime
RUN echo "Europe/Moscow" >  /etc/timezone && date
WORKDIR target/
COPY target/*.jar backend.jar
EXPOSE 8080
CMD java -jar backend.jar --server.port=8080 --server.ssl.key-store=/target/shared/cert/keystore.p12 &> /target/shared/logs/master/`date +%m-%d-%Y_%H:%M:%S`_master.log

